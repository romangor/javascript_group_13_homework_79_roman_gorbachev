const mySQL = require('mysql2/promise');
const config = require('./config');

let connection;

module.exports = {
    async init() {
        connection = await mySQL.createConnection(config.mySQLConfig);
    },
    getConnection() {
        return connection;
    },
}