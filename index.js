const express = require('express');
const categories = require('./app/categories.js');
const places = require('./app/places.js');
const items = require('./app/items.js');
const database = require('./mySQLDB');

const port = 8000;
const app = express();
app.use(express.json());
app.use('/categories', categories);
app.use('/places', places);
app.use('/items', items);


const run = async () => {
    await database.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(e => console.log(e));
