create schema gorbachev collate utf8_general_ci;
use gorbachev;

create table categories
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description text         null
);

create table places
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description text         null
);

create table items
(
    id          int auto_increment
        primary key,
    category_id int          null,
    place_id    int          null,
    name        varchar(255) not null,
    description text         null,
    image       varchar(31)  null,
    constraint items_categories_id_fk
        foreign key (category_id) references categories (id)
        on delete restrict,
    constraint items_places_id_fk
        foreign key (place_id) references places (id)
        on delete restrict
);

insert into categories (id, name, description)
        values  (1, 'furniture', 'soft furniture'),
                (2, 'electronic_devices', 'Description'),
                (3, 'office_supplies', 'Description ');

insert into places (id, name, description)
        values  (1, 'hall', 'some description'),
                (2, 'office_1', 'some description'),
                (3, 'office_2', 'some description'),
                (4, 'room', 'some description');

insert into gorbachev.items (id, category_id, place_id, name, description, image)
values  (1, 2, 3, 'laptop', 'description', '6AvI7-IaF7JZnTLAMW5VY.jpeg'),
        (2, 3, 2, 'stapler', 'description', 'VsCB8djzoY-QvRjZf2JR3.jpeg'),
        (3, 1, 2, 'sofa', 'description', 'tRHoTJd59m9zLVcbBXXyb.jpeg'),
        (4, 1, 4, 'sofa', 'description', '');
