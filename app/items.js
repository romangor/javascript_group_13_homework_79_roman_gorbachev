const express = require('express');
router = express.Router();
const db = require('../mySQLDB');
const path = require("path");
const config = require('../config');
const multer = require('multer');
const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT items.id, items.category_id, items.place_id, items.name FROM items';
        let [items] = await db.getConnection().execute(query);
        return res.send(items);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        let query = 'SELECT items.id, items.category_id, items.place_id, items.name, items.description, items.image FROM items WHERE items.id = ?';
        let [item] = await db.getConnection().execute(query, [req.params.id]);
        if (!item[0]) {
            return res.send({message: 'This id does not exist'});
        }
        return res.send(item[0]);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.category_id || !req.body.place_id || !req.body.name) {
            return res.status(400).send({message: 'Fill in all required fields'});
        }
        let query = 'INSERT INTO items (category_id, place_id, name, description, image) VALUES (?, ?, ?, ?, ?)';
        const item = {
            id: null,
            category_id: req.body.category_id,
            place_id: req.body.place_id,
            name: req.body.name,
            description: req.body.description,
            image: null
        };
        if (req.file) {
            item.image = req.file.filename;
        }
        const [result] = await db.getConnection().execute(query, [
            item.category_id, item.place_id, item.name, item.description, item.image
        ]);
        item.id = result.insertId;
        return res.send({message: 'Created new item ', item})

    } catch (e) {
        next(e);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        let query = 'DELETE FROM items WHERE id = ?';
        await db.getConnection().execute(query, [req.params.id]);
        res.send({message: 'Deleted item'});
    } catch (e) {
        next(e);
    }
});

router.put('/:id', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.category_id || !req.body.place_id || !req.body.name) {
            return res.status(400).send({message: 'Fill in all required fields'});
        }
        let query = 'UPDATE items SET category_id = ?, place_id = ?, name = ?, description = ?, image = ?  WHERE id = ?'
        const item = {
            category_id: req.body.category_id,
            place_id: req.body.place_id,
            name: req.body.name,
            description: req.body.description,
            image: null,
            id: req.params.id
        }
        if (req.file) {
            item.image = req.file.filename;
        }
        await db.getConnection().execute(query, [
            item.category_id, item.place_id, item.name, item.description, item.image, item.id
        ]);
        return res.send({message: 'Update item ', item});
    } catch (e) {
        next(e);
    }
});


module.exports = router;