const express = require('express');
router = express.Router();
const db = require('../mySQLDB');

let place = {
    id: null,
    name: null,
    description: null
}

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT id, name FROM places';
        let [places] = await db.getConnection().execute(query);
        return res.send(places);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        let query = 'SELECT * FROM places WHERE id = ?'
        let [places] = await db.getConnection().execute(query, [req.params.id]);
        const place = places[0];
        return res.send(place);
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({message: 'Place name is required'});
        }
        place = {
            id: null,
            name: req.body.name,
            description: req.body.description
        };
        let query = 'INSERT INTO places (name, description) VALUES (?, ?)'
        const [result] = await db.getConnection().execute(query, [
            place.name,
            place.description
        ]);
        place.id = result.insertId;
        return res.send({message: 'Created new place ', place});
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        let query = 'DELETE FROM places WHERE id = ?';
        await db.getConnection().execute(query, [req.params.id]);
        res.send({message: 'Deleted place'});
    } catch (e) {
        next(e);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({message: 'Place name is required'});
        }
        place = {
            id: req.body.id,
            name: req.body.name,
            description: req.body.description
        };
        let query = 'UPDATE places SET ';
        if (req.body.id) {
            query += 'id = ?, name = ?, description = ? WHERE id = ?';
        } else {
            query += 'name = ?, description = ? WHERE id = ?';
        }
        let data = [place.id, place.name, place.description, req.params.id];
        const filteredData = data.filter(item => {
            return item !== undefined;
        });
        const result = await db.getConnection().execute(query, filteredData);
        if (result[0].affectedRows === 0) {
            return res.status(400).send({message: 'Incorrect id'});
        } else {
            return res.send({message: 'Updated place ', place});
        }
    } catch (e) {
        next(e);
    }
});

module.exports = router;