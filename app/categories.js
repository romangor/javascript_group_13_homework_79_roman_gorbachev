const express = require('express');
router = express.Router();
const db = require('../mySQLDB');

let category = {
    id: null,
    name: null,
    description: null,
};

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT id, name FROM categories';
        let [categories] = await db.getConnection().execute(query);
        return res.send(categories);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        let query = 'SELECT * FROM categories WHERE id = ?'
        let [categories] = await db.getConnection().execute(query, [req.params.id]);
        const category = categories[0];

        return res.send(category);
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({message: 'Category name is required'});
        }
        category = {
            id: null,
            name: req.body.name,
            description: req.body.description,
        };

        let query = 'INSERT INTO categories (name, description) VALUES (?, ?)'
        const [result] = await db.getConnection().execute(query, [
            category.name,
            category.description,
        ]);
        category.id = result.insertId;
        return res.send({message: 'Created new category ', category});
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        let query = 'DELETE FROM categories WHERE id = ?';
        await db.getConnection().execute(query, [req.params.id]);
        res.send({message: 'Deleted category'});
    } catch (e) {
        next(e);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({message: 'Category name is required'});
        }
        category = {
            id: req.body.id,
            name: req.body.name,
            description: req.body.description
        };

        let query = 'UPDATE categories SET ';
        if (req.body.id) {
            query += 'id = ?, name = ?, description = ? WHERE id = ?';
        } else {
            query += 'name = ?, description = ? WHERE id = ?';
        }
        let data = [category.id, category.name, category.description, req.params.id];
        const filteredData = data.filter(item => {
            return item !== undefined;
        });
        const result = await db.getConnection().execute(query, filteredData);
        if (result[0].affectedRows === 0) {
            return res.status(400).send({message: 'Incorrect id'});
        } else {
            return res.send({message: 'Updated category ', place});
        }
    } catch (e) {
        next(e);
    }
});

module.exports = router;